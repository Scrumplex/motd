scrumplex motd
--------------

# Requirements
You will need [update-motd](https://packages.ubuntu.com/bionic/update-motd) or something similar to get this working. There are some requirements per script listed below:

## 00-header
 - figlet
 - lolcat

## 10-systemd
 - systemd
 
## 20-docker
 - docker

## 30-tls
 - openssl

## 40-hetzner
 - curl
 - jq

# Installation

1. Copy the script you want to install to `/etc/update-motd.d`.
2. Customize it's config
3. Test it by running `update-motd`

# Dynamic MOTD on Arch Linux
If you have cronie installed, you can put 
```
*/5 * * * * run-parts /etc/update-motd.d > /etc/motd
```
into your crontab, to update your motd every 5th minute.

# License

Licensed under [GNU GENERAL PUBLIC LICENSE 3.0](LICENSE)
